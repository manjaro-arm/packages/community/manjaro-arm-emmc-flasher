# manjaro-arm-emmc-flasher [DEPRICATED]

Script for installing Manjaro ARM directly to internal eMMC cards from SDCard boot. (Mainly for the Pinebook / Pinebook Pro)

This script is "interactive". It requires user input and confirmations


## Usage
This script will run on every boot when installed. It will flash a Manjaro-ARM image to an eMMC disk as the user defines. 

- Confirm actions
- Select correct eMMC device

## Notes
This script should only be used in proper Manjaro-ARM images. Using it on a running system not inteded for eMMC installs can cause significant data loss or even corruption. 
